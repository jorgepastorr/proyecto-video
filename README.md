# Proyecto-video 

## tuxstream

Proyecto de un pequeño servidor de streaming donde puedes visualizar vídeos, subirlos y reproducir streaming en directo ( con mucho retraso ).

Este proyecto se a creado en un docker basado en  debian con nginx, donde tiene un front y un backend con distintas funcionalidades.

Este contenedor está preparado para servir en localhost, si se quiere acceder desde remoto se tendrán que modificar los ficheros `index.html y recolector.py`

```bash
 docker run --rm --name stream -h stream -v front:/var/www -p 80:80 -p 8080:8080 -p 1935:1935  -d  jorepastorr/stream 
```





## Manual de front 


Tuxstream en el front permite:

- Subir y visualizar videos en formato `mp4`
- visualizar el streaming del canal `show` con id "stream o stream2"



### Subir videos

Para subir un vídeo simplemente se tiene que clicar en examinar y enviar consulta. Automáticamente se subirá y veras el vídeo subido en la sección posterior. 



### Visualizar streaming

Puedes visualizar el streaming enviado por el canal `show` con el id `steam o stream2` . 

Esto se hace mandando el streaming desde `ffmpeg`:

```bash
ffmpeg -f pulse -ac 2 -i default -f x11grab -framerate 30 -video_size 1920x1080 -i :0.0+0,0 -vcodec libx264 -preset veryfast -minrate 750k  -maxrate 750k -bufsize 3968k -vf "scale=640:-1, format=yuv420p" -g 60 -acodec libmp3lame -b:a 96k -ar 44100 -f flv rtmp://localhost/show/stream
```

Después de ejecutar el comando anterior recargar la página y dar play al vídeo. ( tiene un retraso importante 5 o 6 segundos ).



## Manual del Back

El back tiene 3 canales definidos, `live, show y video`



**Canal LIVE**:  Mandas el streaming con ffmpeg, vlc, ... y lo recoges por el mismo canal.

```bash
# mandar streaming de la pantalla con ffmpeg
ffmpeg  -f x11grab -framerate 30 -video_size 1920x1080 -i :0.0+0,0 -vcodec libx264 -preset veryfast -minrate 750k  -maxrate 750k -bufsize 3968k -vf "scale=640:-1, format=yuv420p" -g 60 -acodec libmp3lame -b:a 96k -ar 44100 -f flv rtmp://localhost/live

# recoger con vlc
vlc  rtmp://localhost/live 
```



**Canal SHOW**: Este es el canal mas complejo, ya que se asigna un id a la retransmisión, para luego recogerla `rtmp://localhost/show/<id>` 

```bash
# enviar transmisión
ffmpeg  -f x11grab -framerate 30 -video_size 1920x1080 -i :0.0+0,0 -vcodec libx264 -preset veryfast -minrate 750k  -maxrate 750k -bufsize 3968k -vf "scale=640:-1, format=yuv420p" -g 60 -acodec libmp3lame -b:a 96k -ar 44100 -f flv rtmp://localhost/show/identificador

# recoger transmisión
vlc  http://localhost:8080/hls/identificador.m3u8
```



**Canal VIDEO**: Es un canal que según la hora que conectas retransmite un vídeo o otro.

```bash
vlc  rtmp://localhost/video
```