# TuxStream

web del proyecto tuxstream, que es un pequeño servidor styream donde puedes subir videos, y realizar streaming en directo con diferentes canales.

Este contenedor está preparado para servir en localhost, si se quiere acceder desde remoto se tendrán que modificar los ficheros `index.html y recolector.py`

```bash
 docker run --rm --name stream -h stream -v front:/var/www -p 80:80 -p 8080:8080 -p 1935:1935  -d  jorepastorr/stream 
```

