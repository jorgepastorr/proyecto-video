#!/bin/bash

hora=$(date +%H)

if [ $hora -lt 13 ];then
    ffmpeg -loglevel verbose -re -i /opt/tuxstream/videos/bob.mp4  -vcodec libx264 \
    -vprofile baseline -acodec libmp3lame -ar 44100 \
    -ac 1 -f flv 'rtmp://localhost/video'
elif [ $hora -lt 15 ];then
    ffmpeg -loglevel verbose -re -i /opt/tuxstream/videos/vaiana.mp4  -vcodec libx264 \
    -vprofile baseline -acodec libmp3lame -ar 44100 \
    -ac 1 -f flv 'rtmp://localhost/video'
else
    ffmpeg -loglevel verbose -re -i /opt/tuxstream/videos/minion.mp4  -vcodec libx264 \
    -vprofile baseline -acodec libmp3lame -ar 44100 \
    -ac 1 -f flv 'rtmp://localhost/video'
fi
