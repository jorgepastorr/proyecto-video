#!/bin/bash

# conf rtmp
mkdir -p /opt/tuxstream/{hls,videos}
cp /opt/docker/nginx.conf /etc/nginx/

cp /opt/docker/rtmp /etc/nginx/conf.d/
cp /opt/docker/sitehls /etc/nginx/sites-available/
ln -s /etc/nginx/sites-available/sitehls /etc/nginx/sites-enabled/

cp /opt/docker/videos/* /opt/tuxstream/videos
cp /opt/docker/prog.sh /opt/tuxstream/
chmod +x /opt/tuxstream/prog.sh

# conf cgi-bin
mkdir /var/www/cgi-bin
chmod 755 /var/www/cgi-bin
cp /opt/docker/fcgiwrap.conf /etc/nginx/
cp /opt/docker/default /etc/nginx/sites-available/default
/etc/init.d/fcgiwrap start

# create front
tar xzf /opt/docker/webFront.tgz -C /var/www/.

chown www-data:www-data -R /var/www
chmod +x /var/www/cgi-bin/*