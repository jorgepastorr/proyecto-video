#!/usr/bin/python3

'''Test CGI script.
'''

import sys
import cgi
import json

# Debug
# import cgitb; cgitb.enable()

write = sys.stdout.write
form = cgi.FieldStorage()


write("Content-Type: text/plain; charset=UTF-8\r\n") # or "text/html"...
write('Location: http://localhost/index.html\r\n')
write("\r\n")


fileitem = form["myfile"]
if fileitem.file:
    
    nuevoArchivo = open(f"/var/www/html/videos/{fileitem.filename}", "wb")    
    while True:
        line = fileitem.file.readline()
        if not line: break
        nuevoArchivo.write(line)

    nuevoArchivo.close()



with open('/var/www/html/json/videos.json') as f:
    data = json.load(f)

data['videos'].append(fileitem.filename)

with open('/var/www/html/json/videos.json', 'w') as json_file:
     json.dump(data, json_file)


sys.exit(0)

    