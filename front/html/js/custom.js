// Tratado de documento json para la subida de videos
function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

// mostrar el  subido video een la web
readTextFile("json/videos.json", function(text){
    var data = JSON.parse(text);
    var i, x = '';
    for (i in data.videos) {
        x += '<div class="col-lg-5 order-lg-2 m-auto">';
        x += '<video width="100%"  src=videos/' + data.videos[i]  + ' type="video/mp4" controls></video>';
        x += '</div>';
    }
    document.getElementById("subidos").innerHTML = x;        
});


var player = videojs('hls-example');
player.play();

var player = videojs('hls-example2');
player.play();
