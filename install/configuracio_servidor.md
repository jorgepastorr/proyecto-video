# Tuxstream

Configuración del servidor.

Para la configuración del servidor son necesarios los paquetes:

```bash
 apt install -y nginx libnginx-mod-rtmp fcgiwrap ffmpeg
```

Queremos que el servidor pueda:

- recibir archivos que luego reproducirá el frontend
- crear un live streaming
- retransmitir un video aleatorio según la hora



Primero Establecemos el tamaño máximo de lo ficheros a subir al servidor e incluimos un el archivo de la configuración del modulo rtmp.

*/etc/nginx/nginx.conf*

```bash
...

http {
...
	#  poner un maximo al tamaño de archivo a subir
	client_max_body_size 250M;
...
}
# incluir configuración para rtmp
include /etc/nginx/conf.d/rtmp;
```



El el  archivo configuración rtmp establecemos las rutas que se podrán utilizar, en cada ruta le asignaremos una acción diferente.

*/etc/nginx/conf.d/rtmp*

```bash

rtmp {
        server {
        		# purto por donde escuchara
                listen 1935;
                chunk_size 4096;
				
				# acepta streming y lo emite por la misma ruta
                application live {
                        live on;
                        record off;
                }
				
				# ejecuta el script cada vez que se accede a esta ruta
                application video {
                        live on;
                        record off;
                        exec_pull python3 /opt/tuxstream/sorpresa.py
                }

				# live con ruta/id ejemplo: rtmp://ip/show/stream
                # se recive como rtmp://ip/show/stream.m3u8
                # sirve para lives multiples.
				application show {
        		    live on;
	        	    hls on;
		
		    		hls_path /opt/tuxstream/hls/;
	            	hls_fragment 3;
        	    	hls_playlist_length 60;
	            	# disable consuming the stream from nginx as rtmp
        	    	deny play all;
	        	}
        }
}

```



*/etc/nginx/sites-available/sitehls*

```bash
server {
	listen 8080;

location /hls {
        # Disable cache
        add_header Cache-Control no-cache;

        # CORS setup
        add_header 'Access-Control-Allow-Origin' '*' always;
        add_header 'Access-Control-Expose-Headers' 'Content-Length';

        # allow CORS preflight requests
        if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Max-Age' 1728000;
            add_header 'Content-Type' 'text/plain charset=UTF-8';
            add_header 'Content-Length' 0;
            return 204;
        }

        types {
            application/vnd.apple.mpegurl m3u8;
            video/mp2t ts;
        }

        root /opt/tuxstream/;
    }
}
```

Este archivo trabaja con la ruta `show` convierte el streaming recibido en un archivo en el sistema, en la path `/opt/tuxstream/hls/` .

 **Importante**: el directorio `/opt/tuxstream/hls` se tiene que crear





## Configuración de archivos ejecutables del backend

```bash
root@www:~# apt -y install fcgiwrap
root@www:~# cp /usr/share/doc/fcgiwrap/examples/nginx.conf /etc/nginx/fcgiwrap.conf

root@www:~# vi /etc/nginx/fcgiwrap.conf

location /cgi-bin/ {
  # Disable gzip (it makes scripts feel slower since they have to complete
  # before getting gzipped)
  gzip off;

  # Set the root to /usr/lib (inside this location this means that we are
  # giving access to the files under /usr/lib/cgi-bin)
  # change
  root  /var/www;

  # Fastcgi socket
  fastcgi_pass  unix:/var/run/fcgiwrap.socket;

  # Fastcgi parameters, include the standard ones
  include /etc/nginx/fastcgi_params;

  # Adjust non standard parameters (SCRIPT_FILENAME)
  # change
  fastcgi_param SCRIPT_FILENAME  $document_root$fastcgi_script_name;
}

root@www:~# mkdir /var/www/cgi-bin

root@www:~# chmod 755 /var/www/cgi-bin

root@www:~# vi /etc/nginx/sites-available/default
# add into the [server] section

server {
        .....
        .....
        include fcgiwrap.conf;
}

root@www:~# systemctl restart nginx 
```





## Canales

Los canales donde escuchar son:

```bash
rtmp://ip:1935/live
rtmp://ip:1935/video
rtmp://ip:1935/show/id
```

Y se puede escuchar desde:

```bash
vlc -vvv rtmp://ip:1935/live
vlc -vvv rtmp://ip:1935/video
vlc -vvv http://ip:8080/hls/id
```



```bash
# ejemplo de transmitir un archivo
ffmpeg -loglevel verbose -re -i /home/debian/Videos/record2.mp4  -vcodec libx264 -vprofile baseline -acodec libmp3lame -ar 44100 -ac 1 -f flv 'rtmp://localhost:1935/live'

# ejemplo transmitir pantalla
ffmpeg -f x11grab -framerate 30 -video_size 1920x1080 -i :0.0+0,0 -vcodec libx264 -preset veryfast -minrate 750k  -maxrate 750k -bufsize 3968k -vf "scale=640:-1, format=yuv420p" -g 60 -acodec libmp3lame -b:a 96k -ar 44100 -f flv 'rtmp://localhost/live'
```

