# Manual de front tuxstream

[tuxstream](http://tuxstream.jorgepastorr.tk:50022)

Tuxstream en el front permite:

- Subir y visualizar videos en formato `mp4`
- visualizar el streaming del canal `show` con id "stream o stream2"



## Subir videos

Para subir un vídeo simplemente se tiene que clicar en examinar y enviar consulta. Automáticamente se subirá y veras el vídeo subido en la sección posterior. 



## Visualizar streaming

Puedes visualizar el streaming enviado por el canal `show` con el id `steam o stream2` . 

Esto se hace mandando el streaming desde `ffmpeg`:

```bash
ffmpeg -f pulse -ac 2 -i default -f x11grab -framerate 30 -video_size 1920x1080 -i :0.0+0,0 -vcodec libx264 -preset veryfast -minrate 750k  -maxrate 750k -bufsize 3968k -vf "scale=640:-1, format=yuv420p" -g 60 -acodec libmp3lame -b:a 96k -ar 44100 -f flv rtmp://backstream.jorgepastorr.tk:50022/show/stream
```

Después de ejecutar el comando anterior recargar la página y dar play al vídeo. ( tiene un retraso importante 5 o 6 segundos ).



# Manual del Back

El back tiene 3 canales definidos, `live, show y video`



**Canal LIVE**:  Mandas el streaming con ffmpeg, vlc, ... y lo recoges por el mismo canal.

```bash
# mandar streaming de la pantalla con ffmpeg
ffmpeg  -f x11grab -framerate 30 -video_size 1920x1080 -i :0.0+0,0 -vcodec libx264 -preset veryfast -minrate 750k  -maxrate 750k -bufsize 3968k -vf "scale=640:-1, format=yuv420p" -g 60 -acodec libmp3lame -b:a 96k -ar 44100 -f flv rtmp://tuxstream.jorgepastorr.tk:50022/live

# recoger con vlc
vlc  rtmp://backstream.jorgepastorr.tk:50022/live 
```



**Canal SHOW**: Este es el canal mas complejo, ya que se asigna un id a la retransmisión, para luego recogerla `rtmp://tuxstream.jorgepastorr.tk:50022/show/<id>` 

```bash
# enviar transmisión
ffmpeg  -f x11grab -framerate 30 -video_size 1920x1080 -i :0.0+0,0 -vcodec libx264 -preset veryfast -minrate 750k  -maxrate 750k -bufsize 3968k -vf "scale=640:-1, format=yuv420p" -g 60 -acodec libmp3lame -b:a 96k -ar 44100 -f flv rtmp://backstream.jorgepastorr.tk:50022/show/identificador

# recoger transmisión
vlc  http://hlstream.jorgepastorr.tk:50022/hls/identificador.m3u8
```



**Canal VIDEO**: Es un canal que según la hora que conectas retransmite un vídeo o otro.

```bash
vlc  rtmp://tuxstream.jorgepastorr.tk:50022/video
```