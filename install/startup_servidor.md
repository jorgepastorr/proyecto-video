Se a optado por crear un servidor de bajo nivel partiendo de un modulo de nginx `libnginx-mod-rtmp`, este modulo permite transmisiones con el protocolo rtmp.
Por facilidad de uso y compatibilidad con diferentes distribuciones, se a creado un docker que contiene el servidor.

Puesta en marcha:

```bash
docker run --name stream -h stream --net mynet -p 80:80 -p 8080:8080 -p 1935:1935 -d jorgepastorr/stream
```

En el caso de no querer usar el docker, se intalan los paquetes:

```bash
apt install -y nginx libnginx-mod-rtmp fcgiwrap
```

Donde nginx es el servidor web,  `libnginx-mod-rtmp`  un modula para transmisiones rtmp y `fcgiwrap` un modulo para utilizar archivos ejecutables en nginx ( necesario para el backend y frontend )