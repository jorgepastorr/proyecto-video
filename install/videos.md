---------------------------
**Farem un petit programa per mostrar el videos que hem pujat al servidor**

Tots els membres de l'equip treballarareu amb el mateix codi, per això fareu servir el GITLab com a control de versions.

- El programa ha de mostrar els videos que heu emmagatzemat.

- Per cada video, crearem un simple html on l'incrustarem amb un iframe. En total tindrem 3 planes html ( cada membre de l'equip en farà una amb el seu video)

- tindrem una home amb una petita programació, on mirareu la hora del dia. 
Si és de 08:00 a 02:00, mostrarem l'html 01.
Si és de 02:00 a 04:00, mostrarem l'html 02.
Si és de 04:00 a 10:00, mostrarem l'html 03.
Podeu escollir vosaltres el llenguatge de programació. Ha de ser algo molt senzill

------------------------------

web
http://tuxstream.jorgepastorr.tk:50022

## Html's
- [html mati](https://gitlab.com/jorgepastorr/proyecto-video/-/blob/mati/mati.html)
- [html migdia](https://gitlab.com/jorgepastorr/proyecto-video/-/blob/migdia/migdia.html)
- [html tarda](https://gitlab.com/jorgepastorr/proyecto-video/-/blob/tarda/tarda.html)

## Script funcionamiento

```bash
#!/bin/bash

hora=$(date +%H)

if [ $hora -lt 14 ];then
    echo -e "Content-Type: text/plain; charset=UTF-8"
    echo -e "Location: http://tuxstream.jorgepastorr.tk:50022/mati.html "
    echo -e "\r\n" 
elif [ $hora -lt 16 ];then
    echo -e "Content-Type: text/plain; charset=UTF-8"
    echo -e "Location: http://tuxstream.jorgepastorr.tk:50022/migdia.html "
    echo -e "\r\n"
else
    echo -e "Content-Type: text/plain; charset=UTF-8"
    echo -e "Location: http://tuxstream.jorgepastorr.tk:50022/tarda.html "
    echo -e "\r\n"
fi
```
## Ejcutarlo
La web tiene una seccion sorpresa en el nav superior 